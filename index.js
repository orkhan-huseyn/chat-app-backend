const express = require("express");
const fileUpload = require("express-fileupload");
const http = require("http");
const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());
app.use(fileUpload());
app.use("/static", express.static("public"));

const PORT = process.env.PORT || 8080;
const server = http.createServer(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

require("./src/api")(app);
require("./src/socket")(io);

server.listen(PORT, () => {
  console.log("Chat server is running on *:" + PORT);
});
