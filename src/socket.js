module.exports = (io) => {
  io.on("connection", async (socket) => {
    const { userId } = socket.handshake.auth;

    socket.join(userId);

    socket.on("send message", (message, to) => {
      if (!to) {
        socket.broadcast.emit(
          "receive message",
          message,
          socket.handshake.auth.userId
        );
      } else {
        socket
          .to(to)
          .emit("receive message", message, socket.handshake.auth.userId);
      }
    });

    socket.on("typing", (msg) => {
      io.emit("typing", { id: socket.id, msg: msg });
    });

    socket.on("stopTyping", () => {
      io.emit("stopTyping");
    });

    socket.on("disconnect", () => {
      io.emit("user disconnected", socket.handshake.auth.userId);
    });
  });
};
