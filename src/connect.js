const mongoose = require("mongoose");

const connection = mongoose.createConnection(
  "mongodb+srv://orkhan:orkhan1234@cluster0.2wlkq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);

const Schema = mongoose.Schema;

const UsersSchema = new Schema({
  username: String,
  position: String,
  fullName: String,
  image: String,
  password: String,
  timestamp: { type: Date, default: Date.now },
});

const MessagesSchema = new Schema({
  fromUser: Schema.Types.ObjectId,
  toUser: Schema.Types.ObjectId,
  content: String,
  timestamp: { type: Date, default: Date.now },
});

const SessionsSchema = new Schema({
  userId: Schema.Types.ObjectId,
  accessToken: String,
  timestamp: { type: Date, default: Date.now },
});

const User = connection.model("users", UsersSchema);
const Message = connection.model("message", MessagesSchema);
const Session = connection.model("session", SessionsSchema);

module.exports = { User, Message, Session };
