const crypto = require("crypto");
const path = require("path");
const util = require("util");
const ta = require("time-ago");
const { User, Message, Session } = require("./connect");

const SALT = crypto.randomBytes(16).toString("hex");

module.exports = (app) => {
  app.use((req, res, next) => {
    const allowedServices = ["/login", "/registration", "/upload"];
    const accessToken = req.headers["authorization"];
    if (allowedServices.includes(req.url)) {
      next();
    } else if (!accessToken) {
      res.send({
        error: true,
        message: "Access token is not present",
      });
    } else {
      next();
    }
  });

  app.get("/users", async (req, res) => {
    const accessToken = req.headers["authorization"];
    const session = await Session.findOne({ accessToken }).exec();
    const users = await User.find({ _id: { $ne: session.userId } });

    res.send({
      error: false,
      payload: users.map((user) => ({
        id: user._id,
        username: user.username,
        fullName: user.fullName,
        position: user.position,
        image: user.image,
      })),
    });
  });

  app.get("/users/:id", async (req, res) => {
    const user = await User.findById(req.params.id).exec();
    if (user) {
      res.send({ error: false, message: "Successfull", payload: user });
    } else {
      res
        .status(404)
        .send({ error: true, message: "User with this ID does not exist" });
    }
  });

  app.post("/registration", async (req, res) => {
    const existingUser = await User.findOne({
      username: req.body.username,
    }).exec();

    if (existingUser) {
      res.status(400).send({ error: true, message: "User already exists" });
    } else {
      const hashedPassword = crypto
        .pbkdf2Sync(req.body.password, SALT, 1000, 64, "sha512")
        .toString("hex");

      const user = new User();
      user.username = req.body.username;
      user.position = req.body.position;
      user.fullName = req.body.fullName;
      user.image = req.body.image;
      user.password = hashedPassword;
      await user.save();

      res.status(201).send({
        error: false,
        message: "User created successfully",
      });
    }
  });

  app.post("/login", async (req, res) => {
    const user = await User.findOne({
      username: req.body.username,
    }).exec();

    if (user) {
      const hashedPassword = crypto
        .pbkdf2Sync(req.body.password, SALT, 1000, 64, "sha512")
        .toString("hex");

      if (user.password !== hashedPassword) {
        res.send({
          error: true,
          message: "Password is not correct",
        });
        return;
      }

      const accessToken = crypto.randomBytes(32).toString("base64");
      const session = new Session();
      session.userId = user.id;
      session.accessToken = accessToken;
      await session.save();

      const userInfo = {
        id: user._id,
        username: user.username,
        fullName: user.fullName,
        position: user.position,
        image: user.image,
      };

      res.send({
        error: false,
        payload: { user: userInfo, accessToken },
      });
    } else {
      res.send({
        error: true,
        message: "Username is not valid",
      });
    }
  });

  app.delete("/users/:id", async (req, res) => {
    await User.findByIdAndDelete(req.params.id).exec();
    res.send({ error: false, message: "User was deleted successfully" });
  });

  app.post("/users/:id/messages", async (req, res) => {
    const accessToken = req.headers["authorization"];
    const session = await Session.findOne({ accessToken }).exec();

    const message = new Message();
    message.fromUser = session.userId;
    message.toUser = req.params.id;
    message.content = req.body.content;

    await message.save();
    res
      .status(201)
      .send({ error: false, message: "Message sent successfully" });
  });

  app.get("/users/:id/messages", async (req, res) => {
    const accessToken = req.headers["authorization"];
    const session = await Session.findOne({ accessToken }).exec();
    const userId = req.params.id;

    const messages = await Message.find({
      $or: [
        {
          $and: [{ fromUser: session.userId }, { toUser: userId }],
        },
        {
          $and: [{ fromUser: userId }, { toUser: session.userId }],
        },
      ],
    });

    const processedMessages = messages.map((message) => {
      return {
        id: message._id,
        content: message.content,
        isFromSelf: message.fromUser.toString() === session.userId.toString(),
        time: ta.ago(new Date(message.timestamp)),
      };
    });

    res.send({ error: false, payload: processedMessages });
  });

  app.post("/upload", async (req, res) => {
    const avatar = req.files.avatar;
    const extension = path.extname(avatar.name);

    const allowedExtensions = /\.(png|jpeg|jpg|svg)/;
    if (!allowedExtensions.test(extension)) {
      res.send({
        error: true,
        message: "This file extension is not allowed",
      });
      return;
    }

    const fileName = avatar.md5 + extension;
    const uploadPath = `uploads/${fileName}`;
    try {
      await util.promisify(avatar.mv)("public/" + uploadPath);
      res.send({
        error: false,
        payload: uploadPath,
      });
    } catch (e) {
      res.send({
        error: true,
        message: e.message,
      });
    }
  });
};
